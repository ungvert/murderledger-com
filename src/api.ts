export const apiRoot = process.env.REACT_APP_API_ROOT;

export async function api<T>(url: string): Promise<T> {
    const response = await fetch(apiRoot + url);
    if (!response.ok) {
        throw new Error(response.statusText)
    }
    return response.json() as Promise<T>;
}
