import { PlayerPage } from 'pages/PlayerPage';
import { BuildStatsPage } from 'pages/BuildStatsPage';
import { LeaderboardPage } from 'pages/LeaderboardPage';
import { HomePage } from 'pages/HomePage';
import { VodsPage } from 'pages/VodsPage';
import { AboutPage } from 'pages/AboutPage';
import { WeaponMatrixPage } from 'pages/WeaponMatrixPage';

export const routes = [
    {
        path: "/players/:name/:tab",
        main: () => <PlayerPage />
    },
    {
        path: "/leaderboard",
        main: () => <LeaderboardPage />
    },
    {
        path: "/builds/:dataset",
        main: () => <BuildStatsPage />
    },
    {
        path: "/weapon-matrix",
        main: () => <WeaponMatrixPage />
    },
    {
        path: "/vods",
        main: () => <VodsPage />
    },
    {
        path: "/about",
        main: () => <AboutPage />
    },
    {
        path: "/",
        main: () => <HomePage />
    },
    {
        path: "*",
        main: () => <span>Page not found</span>
    }
];
