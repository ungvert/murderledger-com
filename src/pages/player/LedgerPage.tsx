import { useState, useMemo, useEffect } from 'react';
import { Space, Input, Select, Checkbox, Empty, Spin } from 'antd';
import { useLocation, useHistory } from 'react-router-dom';
import humanizeDuration from 'humanize-duration';

import useInfiniteScroll from 'react-infinite-scroll-hook';
import { useDebounceEffect } from 'util/useDebounceEffect';
import { api } from 'api'
import { Event } from 'components/Event';
import { Event as EventModel } from 'models/event';
import { WeaponsSelect } from 'components/WeaponsSelect';

const { Search } = Input;
const { Option } = Select;

type LedgerPageProps = { name: String };

let isLoading = false;
export const LedgerPage = ({ name }: LedgerPageProps) => {
    const history = useHistory();

    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [events, setEvents] = useState<EventModel[]>([]);
    const [syncDelay, setSyncDelay] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);
    const [hasNextPage, setHasNextPage] = useState<boolean>(true);
    const [q, setQ] = useState<string>(queryParams.get('q') ?? '');
    const [battleSize, setBattleSize] = useState<string>(queryParams.get('battle_size') ?? '');
    const [sortBy, setSortBy] = useState<string>(queryParams.get('sort') ?? 'time');
    const [ownWeapon, setOwnWeapon] = useState<string>(queryParams.get('own_weapon') ?? '');
    const [otherWeapon, setOtherWeapon] = useState<string>(queryParams.get('other_weapon') ?? '');
    const [showKills, setShowKills] = useState<boolean>(isEventFlagSet(queryParams.get('show_kills')));
    const [showDeaths, setShowDeaths] = useState<boolean>(isEventFlagSet(queryParams.get('show_deaths')));
    const [showAssists, setShowAssists] = useState<boolean>(isEventFlagSet(queryParams.get('show_assists')));

    const loadMore: (clear: boolean) => void = async (clear = false) => {
        if (isLoading) {
            return;
        }
        isLoading = true;
        const params = new URLSearchParams()

        if (q) {
            params.append("q", q);
        } else {
            params.delete("q");
        }
        if (battleSize) {
            params.append("battle_size", battleSize);
        } else {
            params.delete("battle_size");
        }

        if (sortBy !== 'time') {
            params.append("sort", sortBy);
        } else {
            params.delete("sort");
        }

        if (ownWeapon) {
            params.append("own_weapon", ownWeapon);
        } else {
            params.delete("own_weapon");
        }

        if (otherWeapon) {
            params.append("other_weapon", otherWeapon);
        } else {
            params.delete("other_weapon");
        }

        if (!showKills) {
            params.append("show_kills", '0');
        } else {
            params.delete("show_kills");
        }

        if (!showDeaths) {
            params.append("show_deaths", '0');
        } else {
            params.delete("show_deaths");
        }

        if (!showAssists) {
            params.append("show_assists", '0');
        } else {
            params.delete("show_assists");
        }

        if (clear) {
            history.replace({ search: params.toString() })
        }

        params.set('skip', clear ? '0' : events.length.toString());
        setLoading(true);
        const result = await api<{ events: EventModel[], sync_delay: number }>(`/players/${name}/events?${params.toString()}`);
        if (clear) {
            setEvents(result.events);
        } else {
            setEvents(prev => [...prev, ...result.events]);
        }
        setHasNextPage(result.events.length === 20);
        setSyncDelay(result.sync_delay);
        setLoading(false);
        isLoading = false;
    };

    const infiniteRef = useInfiniteScroll({
        loading,
        hasNextPage,
        onLoadMore: loadMore,
    });

    useDebounceEffect(
        () => loadMore(true),
        300,
        [q, battleSize, sortBy, ownWeapon, otherWeapon, showKills, showDeaths, showAssists, name],
    );

    useEffect(() => {
        setEvents([]);
    }, [name])

    const syncDelayEl = useMemo(() => {
        if (loading) {
            return null;
        }

        const delay = humanizeDuration(syncDelay * 1000, { units: ["d", "h", "m"], round: true });

        if (syncDelay > 1800) {
            return (
                <div title="Albion API is running behind" style={{ color: 'red' }}>Sync Delay: {delay}</div>
            );
        }

        return (
            <div>Sync Delay: {delay}</div>
        );
    }, [syncDelay, loading]);

    const filters = useMemo(() => {
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'end' }}>
                <Space size="large" direction="horizontal" wrap>
                    <div>
                        <div>Opponent Search</div>
                        <Search
                            value={q}
                            allowClear
                            placeholder="Search for opponent"
                            onChange={e => setQ(e.target.value)} style={{ width: 220 }}
                        />
                    </div>
                    <div>
                        <div>Battle Size</div>
                        <Select
                            value={battleSize}
                            defaultValue=""
                            allowClear
                            style={{ width: 140 }}
                            onSelect={v => setBattleSize(v)}
                            onClear={() => setBattleSize('')}
                        >
                            <Option value="">Any Fight Size</Option>
                            <Option value="1v1">1v1</Option>
                            <Option value="2v2">2v2</Option>
                            <Option value="5v5">5v5</Option>
                            <Option value="zvz">ZvZ</Option>
                        </Select>
                    </div>
                    <div>
                        <div>Order By</div>
                        <Select value={sortBy} defaultValue="time" style={{ width: 100 }} onSelect={v => setSortBy(v)}>
                            <Option value="time">Date</Option>
                            <Option value="fame">Fame</Option>
                        </Select>
                    </div>
                    <WeaponsSelect label={`${name} Weapon`} value={ownWeapon} setValue={setOwnWeapon} />
                    <WeaponsSelect label={`Opponent Weapon`} value={otherWeapon} setValue={setOtherWeapon} />
                    <div>
                        <div><label htmlFor="show_kills">Kills</label></div>
                        <div style={{ height: '32px', display: 'flex', alignItems: 'center' }}>
                            <Checkbox id="show_kills" checked={showKills} onChange={() => setShowKills(!showKills)}></Checkbox>
                        </div>
                    </div>
                    <div>
                        <div><label htmlFor="show_deaths">Deaths</label></div>
                        <div style={{ height: '32px', display: 'flex', alignItems: 'center' }}>
                            <Checkbox id="show_deaths" checked={showDeaths} onChange={() => setShowDeaths(!showDeaths)}></Checkbox>
                        </div>
                    </div>
                    <div>
                        <div><label htmlFor="show_assists">Assists</label></div>
                        <div style={{ height: '32px', display: 'flex', alignItems: 'center' }}>
                            <Checkbox id="show_assists" checked={showAssists} onChange={() => setShowAssists(!showAssists)}></Checkbox>
                        </div>
                    </div>
                </Space>
                {syncDelayEl}
            </div>
        );
    }, [syncDelayEl, showAssists, showDeaths, showKills, otherWeapon, ownWeapon, sortBy, battleSize, q, name]);

    const eventElements = useMemo(() => {
        if (loading && events.length === 0) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                    <Spin size="large" />
                </div>
            );
        }
        if (events.length === 0) {
            return (<Empty style={{ marginTop: '80px' }} />)
        }

        return events.map((event, i) => {
            const ad = i === 3 || (i !== 0 && i % 10 === 0);
            return (<Event event={event} key={event.id} perspective={name} ad={ad} />);
        });
    }, [events, loading, name]);

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            {filters}
            <div ref={infiniteRef as React.LegacyRef<HTMLDivElement>}>
                {eventElements}
            </div>
        </Space>
    );
};

function isEventFlagSet(flag: string | null): boolean {
    if (flag === null || flag === '1') {
        return true;
    }

    return false;
}
