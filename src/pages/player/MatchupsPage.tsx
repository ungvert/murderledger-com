import { useEffect, useState, useMemo } from 'react';
import { Spin, Space } from 'antd';

import { WeaponMatrix } from 'components/WeaponMatrix';
import { WeaponMatrix as WeaponMatrixModel, MatchupStats } from 'models/weapon_matrix';
import { api } from 'api';
import { filterDict } from 'util/filterDict';

type MatchupsPageProps = { name: String };

export const MatchupsPage = ({ name }: MatchupsPageProps) => {
    const [matrix, setMatrix] = useState<WeaponMatrixModel | null>(null);

    useEffect(() => {
        api<{ matrix: WeaponMatrixModel }>(`/players/${name}/weapon-matrix`)
            .then(r => {
                let matrix = r.matrix;

                Object.keys(r.matrix).map(key => {
                    return matrix[key] = filterDict((m: MatchupStats) => {
                        return m.wins + m.losses > 5;
                    }, matrix[key]);
                });

                matrix = filterDict((w: { [key: string]: MatchupStats }) => {
                    return Object.keys(w).length > 1;
                }, matrix);

                setMatrix(matrix);
            });
    }, [name]);

    const weaponMatrixView = useMemo(() => {
        if (matrix === null) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                    <Spin size="large" />
                </div>
            );
        }

        return (
            <WeaponMatrix matrix={matrix} forPlayer />
        )
    }, [matrix]);

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            <span><i>Based on last 1000 1v1 fights.</i></span>
            {weaponMatrixView}
        </Space>
    );
};
