import { useState, useMemo, useEffect } from 'react';
import { Space, Empty, Spin, Col, Row, List } from 'antd';
import { Link } from 'react-router-dom';
import humanizeDuration from 'humanize-duration';

import useInfiniteScroll from 'react-infinite-scroll-hook';
import { useDebounceEffect } from 'util/useDebounceEffect';
import { api } from 'api'
import { Battle } from 'components/Battle';
import { Battle as BattleModel } from 'models/battle';
import { Player2V2BattleInfo, Partner, Weapons } from 'models/player_info';
import { GenericItem } from 'components/Item';

type Battle2v2PageProps = { name: String };

let isLoading = false;
export const Battle2v2Page = ({ name }: Battle2v2PageProps) => {
    const [battles, setBattles] = useState<BattleModel[]>([]);
    const [info, setInfo] = useState<Player2V2BattleInfo | null>(null);
    const [syncDelay, setSyncDelay] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);
    const [hasNextPage, setHasNextPage] = useState<boolean>(true);

    useEffect(() => {
        api<Player2V2BattleInfo>(`/players/${name}/info/2v2`)
            .then(info => {
                setInfo(info)
            });
    }, [name]);

    const loadMore: (clear: boolean) => void = async (clear = false) => {
        if (isLoading) {
            return;
        }
        isLoading = true;
        const params = new URLSearchParams()

        params.set('skip', clear ? '0' : battles.length.toString());
        setLoading(true);
        const result = await api<{ battles: BattleModel[], sync_delay: number }>(`/players/${name}/battles/2v2?${params.toString()}`);
        if (clear) {
            setBattles(result.battles);
        } else {
            setBattles(prev => [...prev, ...result.battles]);
        }
        setHasNextPage(result.battles.length === 20);
        setSyncDelay(result.sync_delay);
        setLoading(false);
        isLoading = false;
    };

    const infiniteRef = useInfiniteScroll({
        loading,
        hasNextPage,
        onLoadMore: loadMore,
    });

    useDebounceEffect(
        () => loadMore(true),
        300,
        [name],
    );

    // TODO: extract component
    const syncDelayEl = useMemo(() => {
        if (loading) {
            return null;
        }

        const delay = humanizeDuration(syncDelay * 1000, { units: ["d", "h", "m"], round: true });

        if (syncDelay > 1800) {
            return (
                <div title="Albion API is running behind" style={{ color: 'red' }}>Sync Delay: {delay}</div>
            );
        }

        return (
            <div>Sync Delay: {delay}</div>
        );
    }, [syncDelay, loading]);

    const filters = useMemo(() => {
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'end' }}>
                <Space size="large" direction="horizontal" wrap>
                    &nbsp;
                </Space>
                {syncDelayEl}
            </div>
        );
    }, [syncDelayEl]);

    const battleElements = useMemo(() => {
        if (loading && battles.length === 0) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                    <Spin size="large" />
                </div>
            );
        }
        if (battles.length === 0) {
            return (<Empty style={{ marginTop: '80px' }} />)
        }

        return battles.map(battle => (<Battle battle={battle} key={battle.id} perspective={name} />));
    }, [battles, loading, name]);

    const infoView = useMemo(() => {
        return (
            <Row gutter={24}>
                <Col span={12}>
                    <List
                        size="small"
                        dataSource={info?.partners}
                        header={(<><strong>Partners</strong> Last 30 Days</>)}
                        bordered
                        renderItem={(partner: Partner) => (
                            <List.Item key={partner.name}>
                                <Link to={`/players/${partner.name}/2v2s`}>{partner.name}</Link>
                                <span>
                                    <Space>
                                        <span><strong>{partner.count}</strong> Fights</span>
                                        <span style={{ display: 'inline-block', width: '120px', textAlign: 'right' }}>
                                            <strong>{(partner.win_rate * 100).toFixed(1)}%</strong> Win Rate
                                        </span>
                                    </Space>
                                </span>
                            </List.Item>
                        )}
                    >
                    </List>
                </Col>
                <Col span={12}>
                    <List
                        size="small"
                        dataSource={info?.weapons}
                        header={(<><strong>Weapons</strong> Last 30 Days</>)}
                        bordered
                        renderItem={(weapons: Weapons) => (
                            <List.Item key={weapons.weapon_1 + weapons.weapon_2}>
                                <span style={{ margin: "-10px 0" }}>
                                    <GenericItem item={weapons.weapon_1} size={38} />
                                    <GenericItem item={weapons.weapon_2} size={38} />
                                </span>
                                <span><strong>{weapons.wins}</strong> wins</span>
                            </List.Item>
                        )}
                    >
                    </List>
                </Col>
            </Row>
        );
    }, [info]);

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            {infoView}
            {filters}
            <div ref={infiniteRef as React.LegacyRef<HTMLDivElement>}>
                {battleElements}
            </div>
        </Space>
    );
};
