import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Tabs, Row, Col } from 'antd';

import { api } from 'api';
import { PlayerInfo } from 'models/player_info';
import { LedgerPage } from 'pages/player/LedgerPage';
import { Battle2v2Page } from 'pages/player/Battle2v2Page';
import { WeaponStatsPage } from 'pages/player/WeaponStatsPage';
import { BuildStatsPage } from 'pages/player/BuildStatsPage';
import { MatchupsPage } from 'pages/player/MatchupsPage';
import { MMRChartPage } from 'pages/player/MMRChartPage';
import { PlayerHeader } from 'components/PlayerHeader';
import { FilthyAd } from 'components/FilthyAd';

export const PlayerPage = () => {
    const history = useHistory();
    const { name, tab } = useParams<{ name: string, tab: string }>();
    const [playerInfo, setPlayerInfo] = useState<PlayerInfo>({ name });

    useEffect(() => {
        setPlayerInfo({ name });
        api<PlayerInfo>(`/players/${name}/info`)
            .then(info => {
                setPlayerInfo(info);
            })
            .catch(err => {
                console.log(err);
            });
    }, [name]);

    return (
        <div>
            <Row gutter={10}>
                <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                    <PlayerHeader player={playerInfo} />
                </Col>
                <Col xs={20} sm={24} md={12} lg={12} xl={12}>
                    <div style={{padding: '10px 0'}}>
                        <FilthyAd slot="3788215273" />
                    </div>
                </Col>
            </Row>
            <Tabs
                activeKey={tab}
                defaultActiveKey={tab}
                onChange={t => history.replace(`/players/${name}/${t}`)}
            >
                <Tabs.TabPane tab="Ledger" key="ledger">
                    {tab === 'ledger' ? <LedgerPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="2v2s" key="2v2s">
                    {tab === '2v2s' ? <Battle2v2Page name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Weapons" key="weapons">
                    {tab === 'weapons' ? <WeaponStatsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Builds" key="builds">
                    {tab === 'builds' ? <BuildStatsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Matchups (1v1)" key="matchups">
                    {tab === 'matchups' ? <MatchupsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="MMR Chart" key="mmr-chart">
                    {tab === 'mmr-chart' ? <MMRChartPage name={name} /> : null}
                </Tabs.TabPane>
            </Tabs>
        </div>
    );
}
