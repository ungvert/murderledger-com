import { useEffect, useState, useMemo } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { Table, Spin, Select, Space } from 'antd';

import { BuildStats, BuildStat, Build, buildId } from 'models/build_stats';
import { GenericItem } from 'components/Item'
import { api } from 'api';
import { useWeaponOptions } from 'models/weapons';
import { FilthyAd } from 'components/FilthyAd';
import { WeaponsSelect } from 'components/WeaponsSelect';

export const BuildStatsPage = () => {
    const history = useHistory();
    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [builds, setBuilds] = useState<BuildStat[] | null>(null);
    const [filteredBuilds, setFilteredBuilds] = useState<BuildStat[] | null>(null);
    const { dataset } = useParams<{ dataset: string }>();
    const [weapon, setWeapon] = useState<string>(queryParams.get('weapon') ?? '');

    useEffect(() => {
        api<BuildStats>(`/builds/${dataset}`)
            .then(stats => {
                setBuilds(stats.builds);
            });
    }, [dataset]);

    useEffect(() => {
        if (builds === null) {
            return;
        }

        if (weapon === '') {
            setFilteredBuilds(builds);
        } else {
            setFilteredBuilds(builds.filter(b => b.build.main_hand.type === weapon));
        }

        const params = new URLSearchParams()
        if (weapon !== '') {
            params.append("weapon", weapon);
        } else {
            params.delete("weapon");
        }
        history.replace({ search: params.toString() });
    }, [weapon, builds, history]);

    const weaponOptions = useWeaponOptions(builds);

    const filters = useMemo(() => {
        const changeDataset = (d: string) => {
            history.replace({ pathname: `/builds/${d}` });
        };

        return (
            <Space wrap>
                <div>
                    <div>Data Set</div>
                    <Select
                        value={dataset}
                        defaultValue=""
                        style={{ width: 240 }}
                        onSelect={v => changeDataset(v)}
                    >
                        <Select.Option value="1v1_stalker_7_day">1v1 Stalker Last 7 Days</Select.Option>
                        <Select.Option value="1v1_high_ip_7_day">1v1 Slayer Last 7 Days</Select.Option>
                        <Select.Option value="1v1_high_elo_7_day">1v1 High Rank Last 7 Days</Select.Option>
                    </Select>
                </div>
                <WeaponsSelect options={weaponOptions} value={weapon} setValue={setWeapon} />
            </Space>
        );
    }, [weapon, dataset, history, weaponOptions]);

    if (filteredBuilds === null) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                <Spin size="large" />
            </div>
        );
    }

    return (
        <Space direction="vertical" style={{ width: '100%', marginTop: '20px' }}>
            <FilthyAd slot="3788215273" />
            {filters}
            <Table
                pagination={false}
                scroll={{ x: true }}
                dataSource={filteredBuilds}
                size="small"
                rowKey={(stat: BuildStat) => buildId(stat.build)}
            >
                <Table.Column
                    title="Build"
                    dataIndex="build"
                    key="build"
                    align="center"
                    render={(build: Build) => (
                        <Space >
                            <GenericItem item={build.main_hand.type} size={50} tooltip={build.main_hand.en_name} />
                            <GenericItem item={build.off_hand.type} size={50} tooltip={build.off_hand.en_name} />
                            <GenericItem item={build.head.type} size={50} tooltip={build.head.en_name} />
                            <GenericItem item={build.body.type} size={50} tooltip={build.body.en_name} />
                            <GenericItem item={build.shoe.type} size={50} tooltip={build.shoe.en_name} />
                            <GenericItem item={build.cape.type} size={50} tooltip={build.cape.en_name} />
                        </Space>
                    )}
                />
                <Table.Column
                    title="Usages"
                    dataIndex="usages"
                    key="usages"
                    sorter={(a: BuildStat, b: BuildStat) => a.usages - b.usages}
                    defaultSortOrder="descend"
                />
                <Table.Column
                    title="Fame Ratio"
                    dataIndex="fame_ratio"
                    key="fame_ratio"
                    sorter={(a: BuildStat, b: BuildStat) => (a.fame_ratio ?? 0) - (b.fame_ratio ?? 0)}
                />
                <Table.Column
                    title="Win Rate"
                    dataIndex="win_rate"
                    key="win_rate"
                    render={(v: number) => (v * 100).toFixed(1).toString() + '%'}
                    sorter={(a: BuildStat, b: BuildStat) => a.win_rate - b.win_rate}
                />
                <Table.Column
                    title="Kills"
                    dataIndex="kills"
                    key="kills"
                    sorter={(a: BuildStat, b: BuildStat) => a.kills - b.kills}
                />
                <Table.Column
                    title="Deaths"
                    dataIndex="deaths"
                    key="deaths"
                    sorter={(a: BuildStat, b: BuildStat) => a.deaths - b.deaths}
                />
                <Table.Column
                    title="Assists"
                    dataIndex="assists"
                    key="assists"
                    sorter={(a: BuildStat, b: BuildStat) => a.assists - b.assists}
                />
                <Table.Column
                    title="Kill Fame"
                    dataIndex="kill_fame"
                    key="kill_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: BuildStat, b: BuildStat) => a.kill_fame - b.kill_fame}
                />
                <Table.Column
                    title="Death Fame"
                    dataIndex="death_fame"
                    key="death_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: BuildStat, b: BuildStat) => a.death_fame - b.death_fame}
                />
                <Table.Column
                    title="Average IP"
                    dataIndex="average_item_power"
                    key="average_item_power"
                    render={(v: number) => v.toFixed(0)}
                    sorter={(a: BuildStat, b: BuildStat) => a.average_item_power - b.average_item_power}
                />
            </Table>
        </Space>
    );
};
