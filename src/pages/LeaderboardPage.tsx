import { useEffect, useState, useMemo } from 'react';
import { Table, Spin, Space, Input } from 'antd';
import { useHistory, useLocation, Link } from 'react-router-dom';
import humanizeDuration from 'humanize-duration';

import { useDebounceEffect } from 'util/useDebounceEffect';
import { getRelativeTime } from 'util/relativeTime';
import { Leaderboard, Player } from 'models/leaderboard';
import { api } from 'api';
import { GenericItem } from 'components/Item';
import { TwitchLink } from 'components/TwitchLink';
import { FilthyAd } from 'components/FilthyAd';
import { WeaponsSelect } from 'components/WeaponsSelect';

export const LeaderboardPage = () => {
    const history = useHistory();

    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [leaderboard, setLeaderboard] = useState<Leaderboard | null>(null);
    const [q, setQ] = useState<string>(queryParams.get('q') ?? '');
    const [debouncedQ, setDebouncedQ] = useState<string>(q);
    const [weapon, setWeapon] = useState<string>(queryParams.get('weapon') ?? '');

    useEffect(() => {
        const params = new URLSearchParams()

        if (debouncedQ !== '') {
            params.append("q", debouncedQ);
        } else {
            params.delete("q");
        }

        if (weapon !== '') {
            params.append("weapon", weapon);
        } else {
            params.delete("weapon");
        }

        history.replace({ search: params.toString() })

        params.set('take', '100');
        api<Leaderboard>(`/leaderboards/1v1?${params.toString()}`)
            .then(lb => {
                setLeaderboard(lb);
            }); // TODO: Handle error
    }, [debouncedQ, weapon, history]);

    useDebounceEffect(
        () => setDebouncedQ(q),
        500,
        [q],
    );

    const syncDelayEl = useMemo(() => {
        if (!leaderboard) {
            return null;
        }

        const delay = humanizeDuration(leaderboard.sync_delay * 1000, { units: ["d", "h", "m"], round: true });

        if (leaderboard.sync_delay > 1800) {
            return (
                <div title="Albion API is running behind" style={{ color: 'red' }}>Sync Delay: {delay}</div>
            );
        }

        return (
            <div>Sync Delay: {delay}</div>
        );
    }, [leaderboard]);

    const filters = useMemo(() => (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'end' }}>
            <Space size="large" direction="horizontal" wrap>
                <div>
                    <div>Search</div>
                    <Input.Search
                        value={q}
                        allowClear
                        placeholder="Search for player"
                        onChange={e => setQ(e.target.value)} style={{ width: 220 }}
                    />
                </div>
                <WeaponsSelect value={weapon} setValue={setWeapon} />
            </Space>
            {syncDelayEl}
        </div>
    ), [q, weapon, syncDelayEl]);

    if (!leaderboard) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                <Spin size="large" />
            </div>
        );
    }

    return (
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
            <Space direction="vertical" style={{ maxWidth: '1100px', width: '100%' }}>
                <FilthyAd slot="3788215273" />
                <h1>1v1 Leaderboard</h1>
                {filters}
                <Table size="small" pagination={false} dataSource={leaderboard.data} rowKey={(row: Player) => row.name}>
                    <Table.Column
                        title="Rank"
                        dataIndex="rank"
                        key="rank"
                        align="center"
                    />
                    <Table.Column
                        title="Player"
                        dataIndex="name"
                        key="name"
                        render={(name: string, player: Player) => (
                            <Space>
                                <GenericItem size={35} item={player.favorite_weapon_item} />
                                <Link to={`/players/${name}/ledger`}>{name}</Link>
                                {player.twitch_username ? (<TwitchLink link={`https://twitch.tv/${player.twitch_username}`} />) : null}
                            </Space>
                        )}
                    />
                    <Table.Column
                        title="Rating"
                        dataIndex="rating"
                        key="rating"
                    />
                    <Table.Column
                        title="Last Update"
                        dataIndex="last_update"
                        key="last_update"
                        render={timestamp => getRelativeTime(new Date(timestamp * 1000))}
                    />
                </Table>
            </Space>
        </div>
    );
};
