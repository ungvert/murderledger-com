import { Event } from 'models/event'

export interface Home {
    juicy_kills: Event[];
    high_rank_cds: Event[];
    streamed_fights: Event[];
    last_update: Date;
}
