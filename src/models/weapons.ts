import { useEffect, useState } from 'react';
import { BuildStats } from './build_stats';
import { WeaponMatrix } from './weapon_matrix';

export const weapons: { [id: string]: string } = {
    "": "Any",
    "MAIN_ARCANESTAFF": "Arcane Staff",
    "MAIN_AXE": "Battleaxe",
    "2H_DUALAXE_KEEPER": "Bear Paws",
    "MAIN_ROCKMACE_KEEPER": "Bedrock Mace",
    "2H_IRONGAUNTLETS_HELL": "Black Hands",
    "2H_COMBATSTAFF_MORGANA": "Black Monk Stave",
    "2H_INFERNOSTAFF_MORGANA": "Blazing Staff",
    "2H_NATURESTAFF_HELL": "Blight Staff",
    "MAIN_RAPIER_MORGANA": "Bloodletter",
    "2H_DUALCROSSBOW_HELL": "Boltcasters",
    "2H_BOW_KEEPER": "Bow of Badon",
    "2H_BOW": "Bow",
    "2H_DAGGER_KATAR_AVALON": "Bridled Fury",
    "2H_FIRESTAFF_HELL": "Brimstone Staff",
    "MAIN_SWORD": "Broadsword",
    "2H_MACE_MORGANA": "Camlann Mace",
    "2H_HALBERD_MORGANA": "Carrioncaller",
    "2H_CLEAVER_HELL": "Carving Sword",
    "MAIN_FROSTSTAFF_AVALON": "Chillhowl",
    "MAIN_SCIMITAR_MORGANA": "Clarent Blade",
    "2H_CLAWPAIR": "Claws",
    "2H_CLAYMORE": "Claymore",
    "2H_CROSSBOW": "Crossbow",
    "2H_SKULLORB_HELL": "Cursed Skull",
    "MAIN_CURSEDSTAFF": "Cursed Staff",
    "2H_DAGGERPAIR": "Dagger Pair",
    "MAIN_DAGGER": "Dagger",
    "2H_CURSEDSTAFF_MORGANA": "Damnation Staff",
    "2H_FIRE_RINGPAIR_AVALON": "Dawnsong",
    "MAIN_SPEAR_LANCE_AVALON": "Daybreaker",
    "2H_DUALSICKLE_UNDEAD": "Deathgivers",
    "2H_DEMONICSTAFF": "Demonic Staff",
    "2H_DIVINESTAFF": "Divine Staff",
    "2H_DOUBLEBLADEDSTAFF": "Double Bladed Staff",
    "MAIN_NATURESTAFF_KEEPER": "Druidic Staff",
    "2H_DUALSWORD": "Dual Swords",
    "2H_CROSSBOW_CANNON_AVALON": "Energy Shaper",
    "2H_ENIGMATICSTAFF": "Enigmatic Staff",
    "2H_ARCANE_RINGPAIR_AVALON": "Evensong",
    "2H_HOLYSTAFF_HELL": "Fallen Staff",
    "MAIN_FIRESTAFF": "Fire Staff",
    "2H_DUALHAMMER_HELL": "Forge Hammers",
    "MAIN_FROSTSTAFF": "Frost Staff",
    "2H_DUALSCIMITAR_UNDEAD": "Galatine Pair",
    "2H_GLACIALSTAFF": "Glacial Staff",
    "2H_GLAIVE": "Glaive",
    "2H_QUARTERSTAFF_AVALON": "Grailseeker",
    "2H_ARCANESTAFF": "Great Arcane Staff",
    "2H_CURSEDSTAFF": "Great Cursed Staff",
    "2H_FIRESTAFF": "Great Fire Staff",
    "2H_FROSTSTAFF": "Great Frost Staff",
    "2H_HAMMER": "Great Hammer",
    "2H_HOLYSTAFF": "Great Holy Staff",
    "2H_NATURESTAFF": "Great Nature Staff",
    "2H_RAM_KEEPER": "Grovekeeper",
    "2H_HALBERD": "Halberd",
    "MAIN_HOLYSTAFF_AVALON": "Hallowfall",
    "MAIN_HAMMER": "Hammer",
    "2H_HAMMER_AVALON": "Hand of Justice",
    "2H_CROSSBOWLARGE": "Heavy Crossbow",
    "2H_MACE": "Heavy Mace",
    "MAIN_SPEAR_KEEPER": "Heron Spear",
    "MAIN_FROSTSTAFF_KEEPER": "Hoarfrost Staff",
    "MAIN_HOLYSTAFF": "Holy Staff",
    "2H_ICEGAUNTLETS_HELL": "Icicle Staff",
    "MAIN_MACE_HELL": "Incubus Mace",
    "2H_SCYTHE_HELL": "Infernal Scythe",
    "2H_INFERNOSTAFF": "Infernal Staff",
    "2H_IRONCLADEDSTAFF": "Iron-clad Staff",
    "MAIN_NATURESTAFF_AVALON": "Ironroot Staff",
    "2H_CLAYMORE_AVALON": "Kingmaker",
    "MAIN_CURSEDSTAFF_UNDEAD": "Lifecurse Staff",
    "MAIN_HOLYSTAFF_MORGANA": "Lifetouch Staff",
    "MAIN_1HCROSSBOW": "Light Crossbow",
    "2H_LONGBOW": "Longbow",
    "MAIN_MACE": "Mace",
    "2H_ENIGMATICORB_MORGANA": "Malevolent Locus",
    "2H_BOW_AVALON": "Mistpiercer",
    "2H_FLAIL": "Morning Star",
    "MAIN_NATURESTAFF": "Nature Staff",
    "2H_DUALMACE_AVALON": "Oathkeepers",
    "2H_ARCANESTAFF_HELL": "Occult Staff",
    "2H_ICECRYSTAL_UNDEAD": "Permafrost Prism",
    "2H_SPEAR": "Pike",
    "2H_POLEHAMMER": "Polehammer",
    "2H_QUARTERSTAFF": "Quarterstaff",
    "2H_NATURESTAFF_KEEPER": "Rampant Staff",
    "2H_AXE_AVALON": "Realmbreaker",
    "2H_HOLYSTAFF_UNDEAD": "Redemption Staff",
    "MAIN_CURSEDSTAFF_AVALON": "Shadowcaller",
    "2H_CROSSBOWLARGE_MORGANA": "Siegebow",
    "2H_TWINSCYTHE_HELL": "Soulscythe",
    "MAIN_SPEAR": "Spear",
    "2H_HARPOON_HELL": "Spirithunter",
    "2H_ROCKSTAFF_KEEPER": "Staff of Balance",
    "2H_AXE": "Great Axe",
    "2H_HAMMER_UNDEAD": "Tombhammer",
    "TRASH": "Trash",
    "2H_TRIDENT_UNDEAD": "Trinity Spear",
    "2H_BOW_HELL": "Wailing Bow",
    "2H_WARBOW": "Warbow",
    "2H_REPEATINGCROSSBOW_UNDEAD": "Weeping Repeater",
    "2H_LONGBOW_UNDEAD": "Whispering Bow",
    "2H_WILDSTAFF": "Wild Staff",
    "MAIN_FIRESTAFF_KEEPER": "Wildfire Staff",
    "MAIN_ARCANESTAFF_UNDEAD": "Witchwork Staff",
};

export type WeaponOption = { label: string; value: string };

export const weaponOptions: WeaponOption[] = [
    {
        value: '',
        label: 'Any',
    },
    {
        value: 'MAIN_ARCANESTAFF',
        label: 'Arcane Staff',
    },
    {
        value: 'MAIN_AXE',
        label: 'Battleaxe',
    },
    {
        value: '2H_DUALAXE_KEEPER',
        label: 'Bear Paws',
    },
    {
        value: 'MAIN_ROCKMACE_KEEPER',
        label: 'Bedrock Mace',
    },
    {
        value: '2H_IRONGAUNTLETS_HELL',
        label: 'Black Hands',
    },
    {
        value: '2H_COMBATSTAFF_MORGANA',
        label: 'Black Monk Stave',
    },
    {
        value: '2H_INFERNOSTAFF_MORGANA',
        label: 'Blazing Staff',
    },
    {
        value: '2H_NATURESTAFF_HELL',
        label: 'Blight Staff',
    },
    {
        value: 'MAIN_RAPIER_MORGANA',
        label: 'Bloodletter',
    },
    {
        value: '2H_DUALCROSSBOW_HELL',
        label: 'Boltcasters',
    },
    {
        value: '2H_BOW_KEEPER',
        label: 'Bow of Badon',
    },
    {
        value: '2H_BOW',
        label: 'Bow',
    },
    {
        value: '2H_DAGGER_KATAR_AVALON',
        label: 'Bridled Fury',
    },
    {
        value: '2H_FIRESTAFF_HELL',
        label: 'Brimstone Staff',
    },
    {
        value: 'MAIN_SWORD',
        label: 'Broadsword',
    },
    {
        value: '2H_MACE_MORGANA',
        label: 'Camlann Mace',
    },
    {
        value: '2H_HALBERD_MORGANA',
        label: 'Carrioncaller',
    },
    {
        value: '2H_CLEAVER_HELL',
        label: 'Carving Sword',
    },
    {
        value: 'MAIN_FROSTSTAFF_AVALON',
        label: 'Chillhowl',
    },
    {
        value: 'MAIN_SCIMITAR_MORGANA',
        label: 'Clarent Blade',
    },
    {
        value: '2H_CLAWPAIR',
        label: 'Claws',
    },
    {
        value: '2H_CLAYMORE',
        label: 'Claymore',
    },
    {
        value: '2H_CROSSBOW',
        label: 'Crossbow',
    },
    {
        value: '2H_SKULLORB_HELL',
        label: 'Cursed Skull',
    },
    {
        value: 'MAIN_CURSEDSTAFF',
        label: 'Cursed Staff',
    },
    {
        value: '2H_DAGGERPAIR',
        label: 'Dagger Pair',
    },
    {
        value: 'MAIN_DAGGER',
        label: 'Dagger',
    },
    {
        value: '2H_CURSEDSTAFF_MORGANA',
        label: 'Damnation Staff',
    },
    {
        value: '2H_FIRE_RINGPAIR_AVALON',
        label: 'Dawnsong',
    },
    {
        value: 'MAIN_SPEAR_LANCE_AVALON',
        label: 'Daybreaker',
    },
    {
        value: '2H_DUALSICKLE_UNDEAD',
        label: 'Deathgivers',
    },
    {
        value: '2H_DEMONICSTAFF',
        label: 'Demonic Staff',
    },
    {
        value: '2H_DIVINESTAFF',
        label: 'Divine Staff',
    },
    {
        value: '2H_DOUBLEBLADEDSTAFF',
        label: 'Double Bladed Staff',
    },
    {
        value: 'MAIN_NATURESTAFF_KEEPER',
        label: 'Druidic Staff',
    },
    {
        value: '2H_DUALSWORD',
        label: 'Dual Swords',
    },
    {
        value: '2H_CROSSBOW_CANNON_AVALON',
        label: 'Energy Shaper',
    },
    {
        value: '2H_ENIGMATICSTAFF',
        label: 'Enigmatic Staff',
    },
    {
        value: '2H_ARCANE_RINGPAIR_AVALON',
        label: 'Evensong',
    },
    {
        value: '2H_HOLYSTAFF_HELL',
        label: 'Fallen Staff',
    },
    {
        value: 'MAIN_FIRESTAFF',
        label: 'Fire Staff',
    },
    {
        value: '2H_DUALHAMMER_HELL',
        label: 'Forge Hammers',
    },
    {
        value: 'MAIN_FROSTSTAFF',
        label: 'Frost Staff',
    },
    {
        value: '2H_DUALSCIMITAR_UNDEAD',
        label: 'Galatine Pair',
    },
    {
        value: '2H_GLACIALSTAFF',
        label: 'Glacial Staff',
    },
    {
        value: '2H_GLAIVE',
        label: 'Glaive',
    },
    {
        value: '2H_QUARTERSTAFF_AVALON',
        label: 'Grailseeker',
    },
    {
        value: '2H_ARCANESTAFF',
        label: 'Great Arcane Staff',
    },
    {
        value: '2H_CURSEDSTAFF',
        label: 'Great Cursed Staff',
    },
    {
        value: '2H_FIRESTAFF',
        label: 'Great Fire Staff',
    },
    {
        value: '2H_FROSTSTAFF',
        label: 'Great Frost Staff',
    },
    {
        value: '2H_HAMMER',
        label: 'Great Hammer',
    },
    {
        value: '2H_HOLYSTAFF',
        label: 'Great Holy Staff',
    },
    {
        value: '2H_NATURESTAFF',
        label: 'Great Nature Staff',
    },
    {
        value: '2H_RAM_KEEPER',
        label: 'Grovekeeper',
    },
    {
        value: '2H_HALBERD',
        label: 'Halberd',
    },
    {
        value: 'MAIN_HOLYSTAFF_AVALON',
        label: 'Hallowfall',
    },
    {
        value: 'MAIN_HAMMER',
        label: 'Hammer',
    },
    {
        value: '2H_HAMMER_AVALON',
        label: 'Hand of Justice',
    },
    {
        value: '2H_CROSSBOWLARGE',
        label: 'Heavy Crossbow',
    },
    {
        value: '2H_MACE',
        label: 'Heavy Mace',
    },
    {
        value: 'MAIN_SPEAR_KEEPER',
        label: 'Heron Spear',
    },
    {
        value: 'MAIN_FROSTSTAFF_KEEPER',
        label: 'Hoarfrost Staff',
    },
    {
        value: 'MAIN_HOLYSTAFF',
        label: 'Holy Staff',
    },
    {
        value: '2H_ICEGAUNTLETS_HELL',
        label: 'Icicle Staff',
    },
    {
        value: 'MAIN_MACE_HELL',
        label: 'Incubus Mace',
    },
    {
        value: '2H_SCYTHE_HELL',
        label: 'Infernal Scythe',
    },
    {
        value: '2H_INFERNOSTAFF',
        label: 'Infernal Staff',
    },
    {
        value: '2H_IRONCLADEDSTAFF',
        label: 'Iron-clad Staff',
    },
    {
        value: 'MAIN_NATURESTAFF_AVALON',
        label: 'Ironroot Staff',
    },
    {
        value: '2H_CLAYMORE_AVALON',
        label: 'Kingmaker',
    },
    {
        value: 'MAIN_CURSEDSTAFF_UNDEAD',
        label: 'Lifecurse Staff',
    },
    {
        value: 'MAIN_HOLYSTAFF_MORGANA',
        label: 'Lifetouch Staff',
    },
    {
        value: 'MAIN_1HCROSSBOW',
        label: 'Light Crossbow',
    },
    {
        value: '2H_LONGBOW',
        label: 'Longbow',
    },
    {
        value: 'MAIN_MACE',
        label: 'Mace',
    },
    {
        value: '2H_ENIGMATICORB_MORGANA',
        label: 'Malevolent Locus',
    },
    {
        value: '2H_BOW_AVALON',
        label: 'Mistpiercer',
    },
    {
        value: '2H_FLAIL',
        label: 'Morning Star',
    },
    {
        value: 'MAIN_NATURESTAFF',
        label: 'Nature Staff',
    },
    {
        value: '2H_DUALMACE_AVALON',
        label: 'Oathkeepers',
    },
    {
        value: '2H_ARCANESTAFF_HELL',
        label: 'Occult Staff',
    },
    {
        value: '2H_ICECRYSTAL_UNDEAD',
        label: 'Permafrost Prism',
    },
    {
        value: '2H_SPEAR',
        label: 'Pike',
    },
    {
        value: '2H_POLEHAMMER',
        label: 'Polehammer',
    },
    {
        value: '2H_QUARTERSTAFF',
        label: 'Quarterstaff',
    },
    {
        value: '2H_NATURESTAFF_KEEPER',
        label: 'Rampant Staff',
    },
    {
        value: '2H_AXE_AVALON',
        label: 'Realmbreaker',
    },
    {
        value: '2H_HOLYSTAFF_UNDEAD',
        label: 'Redemption Staff',
    },
    {
        value: 'MAIN_CURSEDSTAFF_AVALON',
        label: 'Shadowcaller',
    },
    {
        value: '2H_CROSSBOWLARGE_MORGANA',
        label: 'Siegebow',
    },
    {
        value: '2H_TWINSCYTHE_HELL',
        label: 'Soulscythe',
    },
    {
        value: 'MAIN_SPEAR',
        label: 'Spear',
    },
    {
        value: '2H_HARPOON_HELL',
        label: 'Spirithunter',
    },
    {
        value: '2H_ROCKSTAFF_KEEPER',
        label: 'Staff of Balance',
    },
    {
        value: '2H_AXE',
        label: 'Great Axe',
    },
    {
        value: '2H_HAMMER_UNDEAD',
        label: 'Tombhammer',
    },
    {
        value: 'TRASH',
        label: 'Trash',
    },
    {
        value: '2H_TRIDENT_UNDEAD',
        label: 'Trinity Spear',
    },
    {
        value: '2H_BOW_HELL',
        label: 'Wailing Bow',
    },
    {
        value: '2H_WARBOW',
        label: 'Warbow',
    },
    {
        value: '2H_REPEATINGCROSSBOW_UNDEAD',
        label: 'Weeping Repeater',
    },
    {
        value: '2H_LONGBOW_UNDEAD',
        label: 'Whispering Bow',
    },
    {
        value: '2H_WILDSTAFF',
        label: 'Wild Staff',
    },
    {
        value: 'MAIN_FIRESTAFF_KEEPER',
        label: 'Wildfire Staff',
    },
    {
        value: 'MAIN_ARCANESTAFF_UNDEAD',
        label: 'Witchwork Staff',
    },
];

export function useWeaponOptions(data: BuildStats['builds'] | WeaponMatrix | null) {
    const [options, setOptions] = useState<WeaponOption[]>([]);

    useEffect(() => {
        if (data === null) {
            return;
        }

        const usedWeapons = Array.isArray(data)
            ? new Set(data.map((b) => b.build.main_hand.type))
            : new Set(Object.keys(data));

        const filteredOptions = weaponOptions.filter(
            ({ value }) => usedWeapons.has(value) || value === ''
        );

        setOptions(filteredOptions);
    }, [data]);

    return options;
}
