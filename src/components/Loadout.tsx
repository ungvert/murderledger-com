import { Loadout as LoadoutModel } from 'models/event';
import { Item } from 'components/Item';

type LoadoutProps = { loadout: LoadoutModel };

export const Loadout = ({ loadout }: LoadoutProps) => (
    <div style={{display: 'flex', flexWrap: 'wrap', paddingTop: '4px'}}>
        <Item item={loadout.main_hand} />
        <Item item={loadout.off_hand} />
        <Item item={loadout.head} />
        <Item item={loadout.body} />
        <Item item={loadout.shoe} />
        <Item item={loadout.cape} />
        <Item item={loadout.bag} />
        <Item item={loadout.food} />
        <Item item={loadout.mount} />
        <Item item={loadout.potion} />
    </div>
);
