import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Layout, Menu } from 'antd';

export const SideBar = () => {
    const [siderCollapsed, setSiderCollapsed] = useState(true);
    const history = useHistory();

    useEffect(() => {
        history.listen(() => {
            setSiderCollapsed(true);
        });
    }, [history]);

    return (
        <Layout.Sider
            className="mobile-sidebar"
            breakpoint="md"
            collapsedWidth="0"
            width={300}
            zeroWidthTriggerStyle={{ top: '14px' }}
            collapsed={siderCollapsed}
            onCollapse={(collapse) => setSiderCollapsed(collapse)}
        >
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                <Menu.Item key="/build-stats/"><Link to="/builds/1v1_high_ip_7_day">Build Stats</Link></Menu.Item>
                <Menu.Item key="/weapon-matrix"><Link to="/weapon-matrix">Weapon Matrix</Link></Menu.Item>
                <Menu.Item key="/vods"><Link to="/vods">VODs</Link></Menu.Item>
                <Menu.Item key="/leaderboard"><Link to="/leaderboard">Leaderboard</Link></Menu.Item>
            </Menu>
        </Layout.Sider>
    );
}
