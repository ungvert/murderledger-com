import { Row, Col, Tooltip, Space } from 'antd';
import { Link } from 'react-router-dom';

import { Battle as BattleModel, BattlePlayer } from 'models/battle';
import { Loadout } from 'components/Loadout';
import { getRelativeTime } from 'util/relativeTime';

type BattleProps = { battle: BattleModel, perspective?: String };

export const Battle = ({ battle, perspective }: BattleProps) => {
    const isWin = battle.winners.findIndex(p => p.name === perspective) !== -1;
    let lefters = isWin ? battle.winners : battle.losers;
    const righters = isWin ? battle.losers : battle.winners;
    const relativeBattleTime = getRelativeTime(new Date(battle.time * 1000));
    const battleTime = (new Date(battle.time * 1000)).toLocaleString();
    const desc = battleDescription(isWin);
    const rowClass = battleClass(isWin);

    return (
        <Row className={`event ${rowClass}`} align="middle" justify="space-between" >
            <Col>
                <Space direction="vertical" size={1}>
                    {lefters.map((player: BattlePlayer) => (
                        <div key={player.name}>
                            <Link to={`/players/${player.name}/ledger`}>
                                <span style={{ fontSize: '1.15em', fontWeight: 'bold' }}>{player.name}</span>
                            </Link>
                            <Loadout loadout={player.loadout} />
                        </div>
                    ))}
                </Space>
            </Col>
            <Col style={{ textAlign: 'center' }}>
                <div>
                    <Space style={{ fontWeight: 'bold', fontSize: '1.15em' }}>
                        {desc}
                    </Space>
                </div>
                <Tooltip title={battleTime}>{relativeBattleTime}</Tooltip>
            </Col>
            <Col>
                <Space direction="vertical" size={1}>
                    {righters.map((player: BattlePlayer) => (
                        <div key={player.name}>
                            <Link to={`/players/${player.name}/ledger`}>
                                <span style={{ fontSize: '1.15em', fontWeight: 'bold' }}>{player.name}</span>
                            </Link>
                            <Loadout loadout={player.loadout} />
                        </div>
                    ))}
                </Space>
            </Col>
        </Row>
    );
};

function battleDescription(isWin: boolean): string {
    if (isWin) {
        return 'Win';
    }

    return 'Loss';
}

function battleClass(isWin: boolean): string {
    if (isWin) {
        return 'kill';
    }

    return 'death';
}
