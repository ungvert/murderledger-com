import { PlayerInfo } from 'models/player_info';
import { GenericItem } from 'components/Item';

type PlayerHeaderProps = {
    player: PlayerInfo
};

export const PlayerHeader = ({ player }: PlayerHeaderProps) => {
    const rank = player.elo_1v1 ? (<div>1v1 Rank: <strong>{player.rank_1v1}</strong> (MMR {player.elo_1v1})</div>) : null;
    const img = player.fav_weapon ? (<GenericItem size={40} item={player.fav_weapon ?? ''} />) : null;

    return (
        <>
            <h1 style={{ fontSize: '35px', fontWeight: 'bolder' }}>{player.name} {img}</h1>
            {rank}
        </>
    );
}
