import { useState, KeyboardEvent } from 'react';
import { Menu, Select } from 'antd';
import { Link, useRouteMatch, useHistory, Route } from 'react-router-dom';
import { BarsOutlined } from '@ant-design/icons';

import { useDebounceEffect } from 'util/useDebounceEffect';
import { api } from 'api'

const { Option } = Select;

export const AppBar = () => {
    const history = useHistory();
    const route = useRouteMatch();
    const [suggestions, setSuggestions] = useState<string[]>([])
    const [q, setQ] = useState<string>('')
    const [loading, setLoading] = useState<boolean>(false);

    const fetchSuggestions = async () => {
        if (q.length < 3) {
            setSuggestions([]);
            return;
        }
        setLoading(true);
        const result = await api<{ results: string[] }>(`/player-search/${q}`);
        setSuggestions(result.results)
        setLoading(false);
    };

    const handleSelect = (name: string) => {
        setQ('');
        history.push(`/players/${name}/ledger`);
    };

    const handleKey = ({ key }: KeyboardEvent) => {
        if (key === 'Enter' && suggestions.length === 0) {
            setQ('');
            history.push(`/players/${q}/ledger`);
        }
    };

    useDebounceEffect(
        () => fetchSuggestions(),
        150,
        [q],
    );

    return (
        <>
            <Link to="/">
                <img src="/logo.png" alt="logo" />
            </Link>
            <div style={{ flexGrow: 1 }}>
                <Menu
                    className="desktop-bar"
                    theme="dark"
                    mode="horizontal"
                    selectedKeys={[route.path]}
                    style={{ backgroundColor: 'rgb(83, 25, 5)' }}
                    overflowedIndicator={(<BarsOutlined style={{fontSize: '1.3em'}} />)}
                >
                    <Menu.Item key="/build-stats/"><Link to="/builds/1v1_high_ip_7_day">Build Stats</Link></Menu.Item>
                    <Menu.Item key="/weapon-matrix"><Link to="/weapon-matrix">Weapon Matrix</Link></Menu.Item>
                    <Menu.Item key="/vods"><Link to="/vods">VODs</Link></Menu.Item>
                    <Menu.Item key="/leaderboard"><Link to="/leaderboard">Leaderboard</Link></Menu.Item>
                </Menu>
            </div>
            <Route render={({ location }) => {
                if (location.pathname !== '/') {
                    return (
                        <Select
                            className="desktop-bar"
                            size="large"
                            placeholder="Player Search"
                            showSearch
                            onSearch={s => setQ(s)}
                            onInputKeyDown={handleKey}
                            onSelect={handleSelect}
                            showArrow={false}
                            loading={loading}
                            style={{ width: '250px', backgroundColor: 'rgba(0,0,0,0.5)' }}
                        >
                            {suggestions.map(name => (<Option key={name} value={name}>{name}</Option>))}
                        </Select>
                    );
                }
                return null;
            }}>
            </Route>
        </>
    );
};
