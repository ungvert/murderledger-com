import { Row, Col, Tag, Tooltip, Space } from 'antd';
import { Link } from 'react-router-dom';
import { ExportOutlined } from '@ant-design/icons';

import { Event as EventModel, Player } from 'models/event';
import { Loadout } from 'components/Loadout';
import { getRelativeTime } from 'util/relativeTime';
import { TwitchLink } from 'components/TwitchLink';
import { FilthyEventAd } from 'components/FilthyEventAd';

type EventProps = { event: EventModel, perspective?: String, ad?: boolean };

const PlayerName = ({ player, link }: { player: Player, link?: boolean }) => {
    const alliance = player.alliance_name !== "" ? `[${player.alliance_name}]` : '';

    const vod = player.vod ? (
        <a href={player.vod} target="_blank" rel="noreferrer">
            <img src="/twitch.png" alt="twitch vod" />
        </a>
    ) : null;

    if (link) {
        return (
            <Space>
                <Link to={`/players/${player.name}/ledger`}>
                    <Space>
                        <span style={{ fontSize: '1.15em', fontWeight: 'bold' }}>{player.name}</span>
                        <span>{alliance} {player.guild_name}</span>
                    </Space>
                </Link>
                <span style={{ fontSize: '1.15em' }}>({player.item_power} IP)</span>
                {vod}
            </Space>
        );
    }

    return (
        <Space>
            <span style={{ fontSize: '1.15em', fontWeight: 'bold' }}>{player.name}</span>
            <span>{alliance} {player.guild_name}</span>
            <span style={{ fontSize: '1.15em' }}>({player.item_power} IP)</span>
            {vod}
        </Space>
    );
};

const EloChange = ({ change }: { change?: number }) => {
    if (!change && change !== 0) {
        return null;
    }

    const color = change >= 0 ? '#44ff44' : '#ff4444';
    const title = change >= 0 ? 'Elo gained' : 'Elo lost';

    return (
        <span title={title} style={{ color: color }}>
            ({change > 0 ? '+' : ''}{change})
        </span>
    );
};

function eventDescription(isKill: boolean, event: EventModel): string {
    if (!isKill) {
        return 'Death';
    }

    if (!event.killer.is_primary) {
        return 'Assist';
    }

    return 'Kill';
}

function getEventClass(isKill: boolean, event: EventModel): string {
    if (!isKill) {
        return 'death';
    }

    if (!event.killer.is_primary) {
        return 'assist';
    }

    return 'kill';
}

export const Event = ({ event, perspective, ad = false }: EventProps) => {
    const isKill = perspective ? perspective.toLowerCase() === event.killer.name.toLowerCase() : true;
    const leftPlayer = isKill ? event.killer : event.victim;
    const rightPlayer = isKill ? event.victim : event.killer;
    const relativeEventTime = getRelativeTime(new Date(event.time * 1000));
    const eventTime = (new Date(event.time * 1000)).toLocaleString();
    const fame = event.total_kill_fame;
    const twitchLink = leftPlayer.vod ? leftPlayer.vod : rightPlayer.vod;
    const icon = perspective ? eventIcon(isKill, event.killer.is_primary) : (<TwitchLink link={twitchLink} />);
    const desc = perspective ? eventDescription(isKill, event) : (<a target="_blank" rel="noreferrer" href={twitchLink}>Killed</a>);
    const eventClass = perspective ? getEventClass(isKill, event) : '';

    return (
        <>
        <Row className={`event ${eventClass}`} >
            <Col flex="none" style={{ fontSize: '1.3em', display: 'flex', alignItems: 'center', padding: '3px' }}>
                <div>{icon}</div>
            </Col>
            <Col flex="none">
                <PlayerName player={leftPlayer} link={!perspective} />
                <Loadout loadout={leftPlayer.loadout} />
            </Col>
            <Col flex="auto" style={{ textAlign: 'center' }}>
                <div>
                    <Space style={{ fontWeight: 'bold', fontSize: '1.15em' }}>
                        {desc}
                        <EloChange change={leftPlayer.elo_change} />
                    </Space>
                </div>
                <Tooltip title={eventTime}>{relativeEventTime}</Tooltip>
                <div>
                    <a
                        title="View Full Event on Albion Killboard"
                        href={`https://albiononline.com/en/killboard/kill/${event.id}`}
                        target="_blank"
                        rel="noreferrer"
                    >
                        {fame.toLocaleString()} Fame <ExportOutlined />
                    </a>
                </div>
                <div>
                    {event.tags['is_1v1'] ? <Tag color="blue">1v1</Tag> : null}
                    {event.tags['is_2v2'] ? <Tag color="gold">2v2</Tag> : null}
                    {event.tags['is_5v5'] ? <Tag color="cyan">5v5</Tag> : null}
                    {event.tags['is_zvz'] ? <Tag color="magenta">zvz</Tag> : null}
                    {event.tags['unfair'] ? <Tag color="red">ip unfair</Tag> : null}
                    {event.tags['fair'] ? <Tag color="green">ip fair</Tag> : null}
                </div>
            </Col>
            <Col flex="none" >
                <PlayerName player={rightPlayer} link />
                <Loadout loadout={rightPlayer.loadout} />
            </Col>
        </Row>
        { ad ? <FilthyEventAd /> : null }
        </>
    );
};

function eventIcon(isKill: Boolean, isPrimary: Boolean): String {
    if (!isKill) {
        return '☠️';
    }

    if (isPrimary) {
        return '⚔️';
    }

    return '🛡️';
}
