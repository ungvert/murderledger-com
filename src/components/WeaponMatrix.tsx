import { useMemo } from 'react';
import chroma from 'chroma-js';

import { GenericItem } from 'components/Item'
import { WeaponMatrix as WeaponMatrixModel } from 'models/weapon_matrix';
import { weapons as weaponDict } from 'models/weapons';

const chromaScale = chroma.scale(['red', '#aaa', 'lime']).padding(-0.15);

type WeaponMatrixProps = {
    matrix: WeaponMatrixModel,
    weapon?: string,
    forPlayer?: boolean,
}

export const WeaponMatrix = ({ matrix, weapon = '', forPlayer = false }: WeaponMatrixProps) => {
    const yWeapons = useMemo(() => {
        let weapons = Object.keys(matrix);

        if (forPlayer) {
            weapons.sort((a, b) => {
                const usages = (w: string) => Object.values(matrix[w]).reduce((u, m) => u + m.wins + m.losses, 0);
                return usages(b) - usages(a);
            });
        }

        return weapons;

    }, [matrix, forPlayer]);

    const xWeapons = useMemo(() => {
        if (!forPlayer) {
            return Object.keys(matrix);
        }

        const weapons: string[] = [];
        Object.values(matrix)
            .map(Object.keys)
            .reduce((w, c) => w.concat(c), [])
            .forEach((w) => {
                if (weapons.indexOf(w) === -1) {
                    weapons.push(w);
                }
            });

        return weapons;

    }, [matrix, forPlayer]);

    const filteredWeapons = useMemo(() => {
        return yWeapons.filter(w => {
            return weapon === '' || w === weapon;
        })
    }, [yWeapons, weapon]);

    const headerRow = (
        <div className="weapon-matrix-header">
            <div style={{ flex: '0 0 50px', height: '50px' }}></div>
            {xWeapons.map(w => (<GenericItem key={w} item={w} size={50} />))}
        </div>
    );

    const viewMatchup = (w1: string, w2: string) => {
        if (w1 === w2 && !forPlayer) {
            return (<span>\</span>);
        }

        if (!matrix[w1][w2]) {
            return (<span>-</span>);
        }
        const matchup = matrix[w1][w2];
        const winRate = matchup.wins / (matchup.wins + matchup.losses) * 100;

        const title = forPlayer
            ? `You win ${winRate.toFixed(0)}% of the time against ${weaponDict[w2]} with ${weaponDict[w1]} (n=${matchup.wins + matchup.losses})`
            : `${weaponDict[w1]} wins ${winRate.toFixed(0)}% of the time against ${weaponDict[w2]} (n=${matchup.wins + matchup.losses})`;

        return (<span title={title} style={{ color: chromaScale(winRate / 100).hex() }}>{winRate.toFixed(0)}%</span>);
    };

    const viewRow = (weapon: string) => {
        return (
            <div key={weapon} style={{ display: 'flex' }}>
                <div style={{ left: '0', position: 'sticky' }}>
                    <GenericItem item={weapon} size={50} />
                </div>
                {xWeapons.map(otherWeapon => (
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flex: '0 0 50px' }} key={`${weapon}-${otherWeapon}`}>
                        {viewMatchup(weapon, otherWeapon)}
                    </div>
                ))}
            </div>
        );
    }
    // view Row

    return (
        <div style={{ width: '100%' }}>
            {headerRow}
            {filteredWeapons.map(viewRow)}
        </div>
    );
};
