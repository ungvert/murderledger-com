import { Select } from 'antd';
import { WeaponOption, weaponOptions } from 'models/weapons';
import React from 'react';

type Props = {
    value: string;
    setValue: (value: React.SetStateAction<string>) => void;
    options?: WeaponOption[];
    label?: string;
};
export const WeaponsSelect: React.FC<Props> = ({
    value,
    setValue,
    options = weaponOptions,
    label = 'Weapon',
}) => {
    return (
        <div>
            <div>{label}</div>
            <Select
                options={options}
                showSearch
                defaultValue=""
                allowClear
                value={value}
                style={{ width: 180 }}
                onSelect={(v) => setValue(v)}
                onClear={() => setValue('')}
                optionFilterProp="label"
                filterOption={true}
            />
        </div>
    );
};
